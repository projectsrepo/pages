const pagination = {
	perpage: 10,
	currentpage: 1,
	totalitems: 0,

	itemsrange: function(index = 1) {
		return (this.currentpage-index) * this.perpage
	},
	totalpages: function() {
		return Math.ceil(this.totalitems/this.perpage)
	},
	paginate: function() {
		switch (true) {
			case (this.totalpages() < 8):
				return [1, 2, 3, 4, 5, 6, 7].slice(0, this.totalpages())

			case (this.currentpage > 4 && this.currentpage < this.totalpages()-3):
				return [1, `(${[pagedown = +this.currentpage-this.perpage, pagedown > 0 ? pagedown : 1][1]})`, this.currentpage-1, this.currentpage, +this.currentpage+1, `(${[pageup = +this.currentpage+this.perpage, pageup > this.totalpages() ? this.totalpages() : pageup][1]})`, this.totalpages()]

			case (this.currentpage > this.totalpages()-4):
				return [1, `(${[pagedown = +this.currentpage-this.perpage, pagedown > 0 ? pagedown : 1][1]})`, this.totalpages()-4, this.totalpages()-3, this.totalpages()-2, this.totalpages()-1, this.totalpages()]

			default:
				return [1, 2, 3, 4, 5, `(${[pageup = +this.currentpage+this.perpage, pageup > this.totalpages() ? this.totalpages() : pageup][1]})`, this.totalpages()]
		}
	}
}

const numerals = {
	eastern: function(text) {
		return String(text).replace(/[0-9]/g, digit => "٠١٢٣٤٥٦٧٨٩".substr(digit, 1))
	},
	western: function(text) {
		return String(text).replace(/[٠-٩]/g, digit => "٠١٢٣٤٥٦٧٨٩".indexOf(digit))
	}
}

const searchuthmani = {
	format: function(word, nonconsonant = "[^ء-غف-يٱ ]*?") {
		return word
			.replace(/[,@\.\[\]\(\)\{\}\-\|\^\!\$\*\+\=\\]/g, "")

			.replace(/([َُِ])([ّٔ])/g, "$2$1")

			.replace(/( و?يا) /g, "$1")
			.replace(/^(و?يا) /g, "$1")

			.replace(/الذ/g, "[@1]")
			.replace(/الل/g, "[@2]")
			.replace(/ا/g, "[@3]")
			.replace(/ي/g, "[@4]")
			.replace(/ى/g, "[@5]")
			.replace(/[أءؤئ]/g, "[@6]")
			.replace(/آ/g, "[@7]")
			.replace(/و/g, "[@8]")
			.replace(/ن/g, "[@9]")
			 
			.replace(/\]([^\[]+?)\[/g, "],$1,[")
			.replace(/^([^\[]+?)\[/g, "$1,[")
			.replace(/\]([^\]]+)$/g, "],$1")
			.replace(/\]\[/g, "],[")

			.split(",")

			.reduce((reduced, entry) => 
				entry.startsWith("[")
				? reduced + entry + nonconsonant
				: reduced + entry.split("").join(nonconsonant) + nonconsonant
			, "")

			.replace(/\[@1\]/g, "ٱلَّذِ")
			.replace(/\[@2\]/g, "((آ|ٱ)للّ|ٱلَّ(?=.[َْٰ]))")
			.replace(/\[@3\]/g, "([اٰٱ]|وٰ|ىٰ)")
			.replace(/\[@4\]/g, "(ا۟[^ ]|[ىيۦـ](?![ٰٔ]))")
			.replace(/\[@5\]/g, "([اۦٰى](?![َّْ]))")
			.replace(/\[@6\]/g, "[ٔئءإؤأ]")
			.replace(/\[@7\]/g, "(ءَا|َٔا|آ|َٔـٰ)")
			.replace(/\[@8\]/g, "[وۥ]")
			.replace(/\[@9\]/g, "(ي۟)?[نۨ]")
	}
}