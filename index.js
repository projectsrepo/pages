﻿function app(data = []) {
	/* template */
	document.body.innerHTML = `
		<header>إِنَّهُ لَقُرْءَانٌ كَرِيمٌ</header>
		<input id="filter" type="search" autocomplete="off" autofocus placeholder="${numerals.eastern('مفردة <توبة>  -أو-  سورة:آية <توبة: | توب: | وبة: | التوبة:1 | :1 | : >')}">
		<p id="total"></p>
		<section id="results"></section>
		<section id="pagination"></section>
	`
	document.body.style.backgroundColor = "#FFFDF7"

	/* variables declaration */
	const input = document.getElementById("filter")
	const p = document.getElementById("total")
	const ol = document.getElementById("results")
	const buttons = document.getElementById("pagination")

	let keyword = ""
	let keynum = ""
	let filtered = []
	let mark = {}

	/* functions declaration */
	const buttonstemplate = paginated => paginated
		.map(entry => `<button value="${entry}">${entry}</button>`)
		.join("")
		.replace(/value=.?\((\d+?)\).?>\(\d+?\)<\/button>/gm, `title="${pagination.perpage} صفحات" value="$1">...</button>`)
		.replace(`>${pagination.currentpage}</button>`, ` disabled>${pagination.currentpage}</button>`)

	const pagetemplate = () =>
		(
			`<ol start="${pagination.itemsrange()+1}">`
			+
			filtered.slice(pagination.itemsrange(), pagination.itemsrange(0))
				.map(entry =>
					`	<li>
							<p>
								${entry.ayah}
								<code data-tip="${entry.anum}:${entry.snum}">(${entry.surah}:${entry.anum})</code>
							</p>
						</li>
						<hr>
					`
					.replace(mark, "$1<mark>$2</mark>")
				)
			.join("")
			+
			`</ol>`
		).replace(/<hr>(\s*?<\/ol>)/gm, "$1")

	const dataupdate = () => {}

	const domupdate = currentpage =>
		dataupdate(
			pagination.currentpage = currentpage,
			[
				p.innerHTML,
				ol.innerHTML,
				buttons.innerHTML
			]
			= [
				`النتائج: ${numerals.eastern(pagination.totalitems)}`,
				pagetemplate(),
				numerals.eastern(buttonstemplate(pagination.paginate()))
			]
		)

	/* body */
	input.addEventListener("input", at => {
		dataupdate(
			input.value = numerals.eastern(at.target.value.replace(/[,@\.\[\]\(\)\{\}\-\|\^\!\$\*\+\=\\]/g, "")),
			[keyword, keynum] = input.value.split(":")
		)

		if (keyword.length > 1 || keynum !== undefined) {
			dataupdate(
				keyword = searchuthmani.format(keyword),
				[filtered, mark] = (keynum === undefined) ?
					[
						data.filter(entry => (new RegExp(keyword)).test(entry.ayah.trim())),
						new RegExp("()(" + keyword.replace(/\^\[\^ء-غف-يٱ \]\*\?/g, "") + ")", "g")
					]
					:
					[
						data.filter(entry => (new RegExp(keyword)).test(entry.surah.trim()) && (keynum === "" || keynum === entry.anum)),
						new RegExp("(\\([^:]*)(" + keyword + "[^:]*:" + keynum + ")", "g")
					]
				,
				pagination.totalitems = filtered.length
			)

			domupdate(1)
		}
		else {
			dataupdate(p.innerHTML = ol.innerHTML = buttons.innerHTML = "")
		}
	})

	buttons.addEventListener("click", at =>
		(at.target.id === "pagination") ? undefined : domupdate(numerals.western(at.target.value))
	)
}

function importdata() {
	const script = document.createElement('script')
	script.async = true
	script.src = "data.js"
	script.onload = () => {
		localStorage.quransreach = JSON.stringify(data)
		app(JSON.parse(localStorage.quransreach))
	}
	document.head.appendChild(script)
}

void function main() {
	if (!localStorage.quransreach) {
		importdata()
	}
	else {
		const data = JSON.parse(localStorage.quransreach)
		data.length===6236 ? app(data) : importdata()
	}
}()